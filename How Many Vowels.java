public class Vowels {
  public static int getCount(String str) {
    int vowelsCount = 0;
    // Your code here
    for(int t=0;t<str.length();t++) { 
			String c=""+str.charAt(t);
			if ("aeiou".contains(c)) {
				vowelsCount++; 
			}
		} 
    return vowelsCount;
  }
}
